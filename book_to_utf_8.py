"""
Скрипт для перевода текста из кодировки windows 1251 в utf-8.

Не относится непосредственно к запущенному приложению, но тоже был нужен для проекта, поэтому он есть в репозитории :)
"""
book_name = input()
with open(book_name, encoding='windows-1251') as f:
    data = f.read()
with open(book_name,'w', encoding='utf-8') as f:
    f.write(data)