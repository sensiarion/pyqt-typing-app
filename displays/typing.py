import pathlib

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QPlainTextEdit, QInputDialog

from components.records_scores_widget import RecordScoresWidget
from components.speed_counting_plain_text_widget import CorrectSpeedCountingPlainText
from database import Database
from utils import BookSnippetChooser
from PyQt5 import uic


class TypingWidget(QWidget):
    """
    Основное по функционалу окно в приложении

    Содержит в себе:
    * вывод текста для повтора на скорость
    * поле для ввода текста
    * вывод скорости печати
    * вывод таблицы рекордов
    """
    gridLayout: QGridLayout
    laybel: QLabel
    plainTextEdit: CorrectSpeedCountingPlainText
    plainTextView: QPlainTextEdit
    type_speed: QLabel
    records_widget: RecordScoresWidget

    end = pyqtSignal()

    def __init__(self, *args, db: Database, typing_lines: int = 10, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = db
        self._typing_lines = typing_lines
        uic.loadUi(pathlib.Path(__file__).parent / 'typing_widget.ui', self)

        # прикрепление layout'а к виджету делает интерфейс масштабируемым
        # (это можно и в qtDesigner сделать, кликнув правой кнопкой на нужный виджет
        # и выбрав ему какой-либо [ен важно какой] layout)
        # Данный шаг нужен для того чтобы интерфейс был масштабируемым (менял свои размеры вслед за окном)
        self.setLayout(self.gridLayout)

        self.snippet_chooser = BookSnippetChooser('war_and_peace.txt')
        self.new_snippet()

        self.plainTextView.setEnabled(False)
        # в виджете для ввода текста у нас есть 2 события (сигнала): событие при изменении скорости печати
        # и событие окончание печати. На оба события нам необходимо реагировать (выводить скорость печати на экран,
        # просить пользователя ввести своё имя для занесения в таблицу рекордов)
        self.plainTextEdit.speed_changed.connect(self.display_speed)
        self.plainTextEdit.typing_ended.connect(self.add_new_record)

        self.records_widget.show_records(self.db)

    def display_speed(self, speed):
        self.type_speed.setText(str(speed))

    def new_snippet(self):
        """
        Загружаем новый текст для проверки скорости печати
        """
        text = self.snippet_chooser.get_snippet(self._typing_lines)
        self.plainTextView.setPlaceholderText(text)

        self.plainTextEdit.change_text(self.plainTextView.placeholderText())

    def restart(self):
        self.new_snippet()

    def add_new_record(self):
        """
        Просим пользователя ввести своё имя и добавляем его в таблицу рекордов
        """
        username, ok = QInputDialog.getText(self, 'Введите ваше имя для рекорда', 'Имя')
        if ok and username:
            # type_record
            self.records_widget.add_new_record(self.db, username, int(self.type_speed.text()))
            self.end.emit()
