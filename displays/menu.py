import pathlib

from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QPushButton


class MenuWidget(QWidget):
    start_button: QPushButton

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # pathlib.Path(__file__).parent / 'menu.ui' позволяет
        # обратиться к файлу menu.ui, лежащему рядом с текущим файлом
        uic.loadUi(pathlib.Path(__file__).parent / 'menu.ui', self)
