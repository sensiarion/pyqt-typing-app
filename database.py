import dataclasses
import datetime
import sqlite3

scripts = {
    'create records table': """create table if not exists records
(
    id    INTEGER not null
        primary key autoincrement
        unique,
    name  TEXT    not null,
    speed INTEGER,
    date  TEXT    not null
);
    """
}


class Record:
    def __init__(
            self,
            id: int,
            name: str,
            speed: int,
            date: str
    ):
        self.date: str = date
        self.speed = speed
        self.name = name
        self.id = id


class Database:
    def __init__(self, db_name: str):
        self.db_name = db_name
        self._conn = sqlite3.connect(db_name)
        self.create_database()

    def create_database(self):
        for script in scripts.values():
            self._conn.execute(script)
        self._conn.commit()

    def cursor(self) -> sqlite3.Cursor:
        return self._conn.cursor()

    def get_records(self) -> list[Record]:
        cursor = self.cursor()
        values = cursor.execute("SELECT id,name,speed,date FROM records ORDER BY speed DESC") \
            .fetchmany(10)  # таким образом можно ограничить кол-во возвращаемых значений
        # с типами всегда проще работать, поэтому делаем класс обёртку
        result = [Record(*i) for i in values]

        return result

    def add_record(self, name: str, speed: int):
        self._conn.execute(
            "INSERT INTO records (name,speed,date) VALUES (?,?,?)",
            (name, speed, datetime.date.today().isoformat())
        )
        self._conn.commit()

    def close(self):
        self._conn.close()
