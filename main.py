import sys
from pathlib import Path

from PyQt5 import uic
from PyQt5.QtWidgets import (QApplication,
    QMainWindow,
    QWidget,
    QDesktopWidget,
    QPushButton, QStackedWidget, QMessageBox, QAction, QMenu
)

from database import Database
from displays.menu import MenuWidget
from displays.typing import TypingWidget


class MainWindow(QMainWindow):
    stackedWidget: QStackedWidget
    menu:QMenu

    def __init__(self):
        super().__init__()
        uic.loadUi('main_interface.ui', self)

        self.db = self.open_database()

        self.menu_widget = MenuWidget()
        self.typing_widget = TypingWidget(db=self.db, typing_lines=1)
        # для переключения между виджетами используем QStackedWidget в который
        # нужно предварительно добавить все наши "экраны"
        self.stackedWidget.addWidget(self.menu_widget)
        self.stackedWidget.addWidget(self.typing_widget)
        self.stackedWidget.setCurrentWidget(self.menu_widget)
        # отдельно советую обратить внимание на выставленный для centralwidget QVerticalLayout,
        # без этого масштабирование работать не будет (на любом виджете)

        self.menu_widget.start_button.clicked.connect(self.open_typing_widget)
        self.typing_widget.end.connect(self.to_menu)

        self.menu.aboutToShow.connect(self.open_about)

    def open_database(self) -> Database:
        # сохраняем базу в директорию пользователя
        db_folder = Path.home() / 'data'
        if not db_folder.exists():
            db_folder.mkdir()
        db_name = db_folder / 'typing_checker_db.sqlite'
        # db_name = 'local.sqlite'  # для тестирования
        return Database(str(db_name))

    def open_typing_widget(self):
        # подменяем текущий отображаемый виджет
        self.stackedWidget.setCurrentWidget(self.typing_widget)
        self.typing_widget.restart()

    def to_menu(self):
        # подменяем текущий отображаемый виджет
        self.stackedWidget.setCurrentWidget(self.menu_widget)

    def open_about(self):
        """
        Открываем отдельное окно со справкой о программе
        """
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Information)
        msg_box.setText(
            'Данная программа позволяет проверить вам свою скорость печати на клавиатуре. '
            'В качестве тестового текста будут использоваться отрывки из произведения Льва Толстого "Война и мир"'
        )
        msg_box.setWindowTitle("О программе")
        msg_box.setStandardButtons(QMessageBox.Ok)

        msg_box.exec()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()

    # данный код позволяет выбрать монитор для запуска (0 - первый монитор и т.д.)
    display_monitor = 0
    monitor = QDesktopWidget().screenGeometry(display_monitor)
    ex.move(monitor.left(), monitor.top())

    ex.show()
    sys.exit(app.exec_())
