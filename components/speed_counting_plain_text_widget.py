import time

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QTextCursor
from PyQt5.QtWidgets import QPlainTextEdit


class SpeedCountingPlainText(QPlainTextEdit):
    # чтобы другие виджеты понимали когда меняется скорость ввода текста,
    # мы создали своё событие (сигнал)
    speed_changed = pyqtSignal(int)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._speed = 0
        # стартовое время ввода
        self._start_type: float | None = None

        self.textChanged.connect(self.measure_speed)

    def speed(self) -> int:
        typed_text = self.toPlainText()
        if not self._start_type:
            self._start_type = time.time()
            return 0
        if typed_text == '':
            self._start_type = None
            return 0

        current_time = time.time()
        delta = current_time - self._start_type

        type_speed_per_min = (len(typed_text) / delta) * 60

        return int(type_speed_per_min)

    def measure_speed(self):
        speed = self.speed()
        # создаем само событие об измении скорости сигнала
        self.speed_changed.emit(speed)


# В дизайн файле можно увидеть, что данный виджет ввода текста создаётся через этот класс (promoted widget)
class CorrectSpeedCountingPlainText(SpeedCountingPlainText):
    """
    Базовый класс умеет рассчитывать скорость печати, однако в рамках задачи нам необходимо
    учитывать только скорость печати по конкретному тексту.

    Данный класс контролирует ввод и незасчитывает символы не по эталонному тексту
    """

    # будем отправлять сигнал в момент когда введённый текст совпадает с эталоном
    typing_ended = pyqtSignal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Мы не можем сразу передать эталонный текст, т.к. виджет создаётся самим QT
        self._correct_text: str | None = None
        # т.к. не хотелось бы рассчитывать скорость после окончания ввода. Добавляем проверочный флаг
        self._typing_ended = False

    def change_text(self, text: str):
        """
        Выставляем эталонный текст, с которым будет сравниваться ввод пользователя
        """
        self._correct_text = text
        self._typing_ended = False
        self.setPlainText('')

    def remove_unmatched(self) -> bool:
        """
        Функция, контролирующая ввод неправильных символов при печати

        Сравнивает полученный от пользователя текст с образцом и при несовпадении удаляет лишний текст

        :return: был ли удалён текст
        """
        if self._typing_ended:
            self.setPlainText(self.toPlainText()[:-1])
            return True
        snippet = self._correct_text
        result = self.toPlainText()
        if not result:
            return False
        if snippet == result:
            self._typing_ended = True
            self.typing_ended.emit()

        last_typed = result[-1]
        if len(result) > len(snippet):
            # пользователь может пытаться вводить и после окончания текста
            self.setPlainText(result[:-1])
            return False

        snippet_letter = snippet[len(result) - 1]
        if last_typed != snippet_letter:
            # затираем введённый символ, если он неправильный
            self.setPlainText(result[:-1])
            self.moveCursor(QTextCursor.MoveOperation.End)
            return True
        return False

    def measure_speed(self):
        """
        Переопределяем метод расчёта скорости чтобы не учитывать символы "не по тексту"
        """
        if not self.remove_unmatched():
            speed = self.speed()
            self.speed_changed.emit(speed)
