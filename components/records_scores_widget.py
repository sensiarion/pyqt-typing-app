import datetime

from PyQt5.QtWidgets import QListWidget

from database import Database


class RecordScoresWidget(QListWidget):
    """
    Виджет таблицы рекордов

    Хороший пример инкапсуляции. Виджет сам знает как выводить элементы и как с ними работать (добавлять).
    Всё что ему нужно для это - инструмент для запроса данных из внешнего источника (объект базы данных)
    """
    def show_records(self, db: Database):
        records = db.get_records()
        formatted = [
            f'{i.name.capitalize()} - {i.speed}; {datetime.date.fromisoformat(i.date).strftime("%Y.%m.%d")}'
            for i in records
        ]
        self.clear()
        self.addItems(formatted)

    def add_new_record(self, db: Database, name: str, speed: int):
        db.add_record(name, speed)
        self.show_records(db)
