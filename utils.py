import random


class BookSnippetChooser:
    """
    Класс для получения некоторого куска текста из файла

    Прелесть подобно разбиения на классы в том что я в любой момент могу унаследоваться
    и добавить новой логики (сделать генерируемый текст для печати, использовать другую литературу, брать текст из api).
    Всё что в таком случае нужно будет изменить в основной программе - указать другое имя класса.
    """
    def __init__(self, book_filename: str):
        self.book_filename = book_filename
        self._data: list[str] | None = None

    def _load_book(self):
        if self._data:
            return
        with open(self.book_filename, encoding='utf-8') as f:
            data = f.read()
            lines = data.splitlines()
            # Не самое лучшая предварительная обработка текста, но какая есть
            # Лучше всего предварительно обработать весь файл и сохранить его,
            # нежели чем выполнять обработку при каждой загрузке как тут
            self._data = [
                line.strip().replace('–', '-').replace('«', '"').replace('»', '"').replace(' ', ' ')
                for line in lines if line.strip()
            ]

    def get_snippet(self, lines: int = 20) -> str:
        self._load_book()

        starts_from = random.randrange(len(self._data) - lines)

        selected = self._data[starts_from: starts_from + lines]
        text = '\n'.join(selected)

        return text
